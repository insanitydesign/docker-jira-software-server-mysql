# Atlassian Jira Software Server with MySQL
This is a simple extension of the Jira Software Docker image with the MySQL Connector/J already embedded.
 
Find out more about Jira at <https://www.atlassian.com/software/jira>

You can find the repository for this Dockerfile at <https://bitbucket.org/insanitydesign/docker-jira-software-server-mysql>
 
## Configuration
For all inherited configuration options check out the base Jira Software Docker image at <https://hub.docker.com/r/insanitydesign/jira-software-server/>
 
## Product Support
For Jira product support go to [support.atlassian.com](http://support.atlassian.com).

## Disclaimer
I am not related to Atlassian or Jira and do not explicitly endorse any relation. This source and the whole package comes without warranty. It may or may not harm your computer, server etc. Please use with care. Any damage cannot be related back to the author. The source has been tested on a virtual environment. Use at your own risk.

## Personal Note
I don't know if this is very useful for a lot of people but I required what is provided here. I hope this proves useful to you... with all its Bugs and Issues ;) If you like it you can give me a shout at [INsanityDesign](https://insanitydesign.com) or let me know via the repository.